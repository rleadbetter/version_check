## [1.29]
NOTE: x64 only
### Added
Firmware for HUB with external sync module
Checks for firmware update with sync
Atmel driver package

### Changed
Jump report updates
Move AHRS plot to right side

### Fixed
Firmware update when connected to node used incorect device struct
Flash dev FW silent crash


## [1.28]
### Fixed
Legacy support for FW updates
Bug fix for session download when block split has no remainder


## [1.27]
### Added
Added waitbar for update download

### Fixed
JRE for flip install


## [1.26]
### Added
Check for flip install
Flip installer
Auto execution of _INSTALL on x64

### Changed
Additonal legacy support in Plot


## [1.25]
### Added
Log timer test script
rCore remove gravity script
Update calibration IN file tool menu
New convert function
Batchisp firmware flash method
Allows for saving of eeprom data
Added hardware ID support
Will get from device and save to athdata struc
AHRS basic animation on stream
Added waitbar to firmware updates
Added waitbar to batch download device loading
Added dynamic beta to AHRS functions
Added wait bar for export
Built in sabel animation to stream figure
AHRS plotting buitin

### Changed
Check for settings validation fail
Move to byte/sample stream for realtime
Update calibration functions
Now use new stream method
Improve button control
Improve getting of cal/id data
Updates to stripchart
Expand and shrink axis limits
Handle input of single data point
Gyro calibration updates for High intensity module
Updates to improve reliability
Unique stream commands for high intensity calibration
Improve batch download calibration/ID getting
If fails don't display sensor
Upgrade to urlread2
Faster/Configurable
Reduced serial timeout
Reduction/Removal of unrequired pauses
General interface speedup
Skip unique_ID when skip_cal
Modified connect callback
Checks serial port status
Checks device before scanning network
Session list update for calibration streams
Extra warning dialogs for calibration fails
difications to sabel animation
So can pass in axis handle to draw on
Removed text box
Update lines insded of re plot
Save AHRS processing results
Saved to new dataset
Firmware update V15
Report Issue moved to gitlab account

### Fixed
AHRS channel selection
Gyro cal channel fixes
Tune correct length for calibration stream
Working correctly over sample rate range
ECG and Pulse sensor stream fixes
Node scan command on connect
Reliability issues with combined cmd method
Fixed batch download for pulse and gps modules
Improvements to node device disconnect/hub connect
Checks if no rply/device off
Auto Scan to refresh list if checks fail
Fixed unwrap plot
Update calibration bug fix
Stop gyro calibration plots when deployed
Missing lib bug fix



## [1.23]
### Added
Auto detect firmware file
Sync markers on plot will scale to max data value
AHRS stream option
Calibrated stream option
AHRS report output
GPS parsing
Map plot of position
GPS Sample counter/sync
Export to CSV option
GPS module firmware added
Unwrapped AHRS plot

### Changed
SABEL Jump updates
Update calibration
Swap x->y for Jim's data (need auto orientation)
Update jump thresholds
Update plot to show height
Update saved report
Waitbar for AHRS calc
Updates to wireless data download
Check for expected number of packets
Plotting update for old/generic data

### Fixed
ECG download optimization
GPS parse fixes
Ignore LOGGING_GPS_H1
AHRS Tomo update
CSV output fix
AHRS/real time fixes
Corruption crash fix
Will now prompt to update corrupt device firmware or allow user to redo settings (depending on corruption)
CSV export athdata check


## [1.22]
### Added
Feature to update device calibration from file (prev. dataset)
PulseSensor support and firmware (Additional PPG channel)
Data recovery feature


## [1.21]
### Fixed
Sync channel magnitude fix


## [1.2]
### Added
Dynamic channels
ECG module support
x64 Version support
Timestamp channel (ch1)
Sync time channel (ch2)
Sync channel (ch3)

### Changed
Move to part download for all sesions
Dynamic ploting function
Dynamic stream function
Dynamic download function
rCore_convert_data dynamic updates

### Fixed
rCore_convert_data dynamic bugs
Part download fix for dynamic channels (partInfo)



## [1.1]
### Added
SABEL Jump Beta release
AHRS test scripts

### Fixed
Sync mark in plots fix



## [1.094]
### Added
Sabel reports menu
Extra error checking in cal. functions
Timeouts to main cal. while loop

### Changed
Decreased time required for cal. orientations
Increased overall cal. time and added scale and NaN checks

### Fixed
Moved device get cal. to after version check
Correctly update node refVoltage fields
Replaced IMSHOW as can not be compiled
High Int. firmware cal. fix
version_check update to work
Batch download fixes for EEPROM cal



## [1.09]
### Added
Software version check feature
EEPROM calibration store
New dynamic gyro calibration
AlanLai primary calibration method

### Changed
NODE download timeout sections



## [1.08]
### Added
Report issue menu item
Start of layout for Analysis tab (Sync)
Analysis Plot selection and channel select
High rate handling

### Changed
Driver update
Modal update for figures, main GUI will wait for close
Error handling for calibration, if fig closed during stream
Start->Loop->Done method for wireless download
Calibration more error handling
Handle close on data selection
Handle incorrect data selection
High intensity module updates

### Fixed
High Intensity detection
Batch download error handle
Correct session fail detection
Correct detection of 0 sent packets
Fixes for single file Dir tree issue
Multi-select download bug fix



## [1.07]
### Added
Node parts download
Splits download into 400 block parts
New settings option RF_ADDR_group
Unique RF address groups
cdc_test tools

### Changed
Settings update for RF_ADDR_group additions
serial updates for printf removal, and CR,LF removal (binary data vs ascii)
getserialPort updates for device scan (Just id commands)
batch download updates
RF group updates
Session fail monitoring
node split download
node download timeout reduction

### Fixed
sessionStart_offset fix for node and hub
Driver update, Should fix 32bit machine issues
menu firmware fixes
menu dev update fixes



## [1.06]
### Added
Color change on device tooltip when battery crit
Autoplot option, to auto plot after download
Power off on right-click disconnect
Greater then one day session support

### Changed
Warning on firmware update about erase
Removed test tab
Added some tooltips to buttons
Added plot button
Change default plots to subplots
Keep awake with silent recan on wireless batch

### Fixed
Fix convert function



## [1.04]
### Added
New config.xml fields
Saves window position on exit, Also saves view options
Quit menu item
Restore view menu item
About menu item

### Changed
Hide center panel view option
Save downloaded data as double



## [1.02]
### Added
Data download based on blocks to firmware

### Changed
For special case downloads.
Started adding > day session downloads
Needs to be done in parts
Matlab runs out of memory, and would be better to split files.

Wired batch download changes
Opens all ports on start press
Keeps all devices on if they are connected
Some cleaning



## [1.00]
### Added
View menu
Hide center panel toggle
Adjust UI elements future
Wired download check
Bytes expected vs bytes received
uniqueid from java libs
Battery level in percent
Use VBUS to flag charging
Added batch download to tools menu
Download retry added

### Changed
rCore_convert_data optimization's
All converts have been replaced with vectored operation
Last 8 bytes from 2048 buffer removed from write (logging_buffer)
510 bytes per block
Removed all terminal printf/disp
Replaced with statusStr that writes to status window
NODE Buttons enable/disable on NODE connection
Clear session list on erase
Get serial port modifed to have option of just scaning
Does not connect to any devices, just gets active COM ports

### Fixed
Stream sample rate fix (calibration/realtime/etc.)
Fs set correctly to HUB or NODE setting
Close serial on exit fix



## [0.99]
### Added
Install drivers menu option
Dev firmware flash menu option (no checks)
Max session warning

### Changed
Folder changes
Drivers + Bootloader drivers
Monitor script updates
Update for added flags (restart_log, rf_pwr)
Data conversion optimizations (vector operations)

### Fixed
Firmware flash for deployed working
Fixed tree not working on load issue



## [0.98]
### Added
New debug bootloader options

### Changed
Get usb port optimizations
Removed convert alternatives
Structure upgrade complete